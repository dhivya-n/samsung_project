var express = require('express');
var router = express.Router();

var db = require('../queries');


router.get('/api/student', db.getAllStudents);
router.get('/api/student/:id', db.getSingleStudent);
router.post('/api/student', db.createStudent);
router.put('/api/student/:id', db.updateStudent);
router.delete('/api/student/:id', db.removeStudent);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;


