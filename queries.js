var promise = require('bluebird');

var options = {
  // Initialization Options
  promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://localhost:5432/dhivya';
var db = pgp(connectionString);

// add query functions

module.exports = {
  getAllStudents: getAllStudents,
 getSingleStudent: getSingleStudent,
 createStudent: createStudent,
 updateStudent: updateStudent,
 removeStudent: removeStudent
};
function getAllStudents(req, res, next) {
    db.any('select * from student')
      .then(function (data) {
        res.status(200)
          .json({
            status: 'success',
            data: data,
            message: 'Retrieved ALL Students'
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }
  function getSingleStudent(req, res, next) {
   
    db.one('select * from student where id = $1', req.params.id)
      .then(function (data) {
        res.status(200)
          .json({
            status: 'success',
            data: data,
            message: 'Retrieved ONE Student'
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }




  function createStudent(req, res, next) {
    req.body.age = parseInt(req.body.age);
    db.none('insert into student(id,name, class, age, sex)' +
        'values(${id},${name}, ${class}, ${age}, ${sex})',
      req.body)
      .then(function () {
        res.status(200)
          .json({
            status: 'success',
            message: 'Inserted one Student'
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }
  function updateStudent(req, res, next) {
    db.none('update student set name=$1, class=$2, age=$3, sex=$4 where id=$5',
      [req.body.name, req.body.breed, parseInt(req.body.age),
        req.body.sex,req.params.id])
      .then(function () {
        res.status(200)
          .json({
            status: 'success',
            message: 'Updated Student'
          });
      })
      .catch(function (err) {
        return next(err);
      });
  }
  function removeStudent(req, res, next) {
   
    db.result('delete from student where id = $1', req.params.id)
      .then(function (result) {
        /* jshint ignore:start */
        res.status(200)
          .json({
            status: 'success',
            message: `Removed ${result.rowCount} Student`
          });
        /* jshint ignore:end */
      })
      .catch(function (err) {
        return next(err);
      });
  }